﻿using BusStation.Data;
using BusStation.Models;
using BusStation.Repositories.Implementations;
using BusStation.Repositories.Interfaces;
using BusStation.Transactions.Logging;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Windows;

namespace BusStation.Transactions
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private TextBoxWriter _textBoxWriter;
        public IUnitOfWork UnitOfWork { get; set; }
        BusStationContext BusStationContext { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            InitializeLogging();

            var dbOptions = new DbContextOptionsBuilder<BusStationContext>()
                .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddProvider(new TextBoxLoggerProvider(_textBoxWriter))))
                .Options;
            BusStationContext = new BusStationContext(dbOptions);
           
            UnitOfWork = new UnitOfWork(BusStationContext);
            BusesTable.ItemsSource = UnitOfWork.BusRepository.GetAll().OrderBy(b => b.BusId).ToList();
        }

        private void InitializeLogging()
        {
            // Create a TextBoxWriter and redirect the standard output to it
            _textBoxWriter = new TextBoxWriter(LoggingTextBox);
            Console.SetOut(_textBoxWriter);

            // Other initialization code...
        }

        private void BusesTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedBus = BusesTable.SelectedItem as Bus;
            if (selectedBus != null)
            {
                EditableBrand.Text = selectedBus.BusBrand;
                EditableCapacityBox.Text = selectedBus.Capacity.ToString();
            }
        }

        private void ExecuteFilterButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // call to SP
                var filteredBuses = BusStationContext.Buses.FromSql(@$"FilterBuses {FilterByBrandCombobox.Text}, {FilterByCapacityTextBox.Text}").ToList();
                BusesTable.ItemsSource = filteredBuses;
            }
            catch (SqlException ex)
            {
                MessageBox.Show($"{ex.Message}", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private  void UpdateBusButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedBus = BusesTable.SelectedItem as Bus;
           
            if (selectedBus != null)
            {
                LoggingTextBox.AppendText($"Selected bus with id {selectedBus.BusId}");
                if(StrategyComboBox.Text == "Песимістична")
                {
                    HandlePessimisticStrategy(selectedBus);
                }
                else
                {
                    HandleOptimisticStrategy(selectedBus);
                }
            }
        }

        private void HandlePessimisticStrategy(Bus selectedBus)
        {
            // Store the original values before any changes
            var originalCapacity = selectedBus.Capacity;
            var originalBrand = selectedBus.BusBrand;

            using (var transaction = BusStationContext.Database.BeginTransaction(System.Data.IsolationLevel.Serializable))
            {
                LoggingTextBox.AppendText($"Transaction {transaction.TransactionId} started\n");
                try
                {
                    var capacity = EditableCapacityBox.Text;
                    var brand = EditableBrand.Text;

                    // blocking the row for changes from another transactions
                    BusStationContext.Database.ExecuteSqlInterpolated($"SELECT * FROM Buses WITH (UPDLOCK) WHERE BusId = {selectedBus.BusId}"); 
                    BusStationContext.Database.ExecuteSqlInterpolated($"UPDATE Buses SET BusBrand = {brand}, Capacity = {capacity} WHERE BusId = {selectedBus.BusId}");

                    BusStationContext.SaveChanges();
                    transaction.Commit();
                    
                    LoggingTextBox.AppendText($"Transaction {transaction.TransactionId} was finished\n");
                    BusesTable.ItemsSource = UnitOfWork.BusRepository.GetAll().OrderBy(b => b.BusId).ToList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show($"{ex.Message}", "Помилка!", MessageBoxButton.OK, MessageBoxImage.Error);

                    // Rollback the transaction
                    transaction.Rollback();
                    LoggingTextBox.AppendText($"Transaction rollback\n");
                    EditableCapacityBox.Text = originalCapacity.ToString();
                    EditableBrand.Text = originalBrand;
                }
            }
        }

        private void HandleOptimisticStrategy(Bus selectedBus)
        {
            var bus = BusStationContext.Buses.Find(selectedBus.BusId);

            var capacity = EditableCapacityBox.Text;
            var brand = EditableBrand.Text;
            
            if(!int.TryParse(capacity, out var busCapacity))
            {
                MessageBox.Show("Error converting data type nvarchar to int", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            bus.BusBrand = brand;
            bus.Capacity = busCapacity;
            bool saveFailed;
            do
            {
                saveFailed = false;

                try
                {
                    BusStationContext.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    MessageBox.Show($"Автобус був вже оновлений іншим користувачем!.\n{ex.Message}", "Конфлікт паралельності!", MessageBoxButton.OK, MessageBoxImage.Error);

                    // Update original values from the database, keep client values (rollback)
                    var entry = ex.Entries.Single();
                    entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    LoggingTextBox.AppendText($"Transaction rollback\n");
                }
            } while (saveFailed);
            BusesTable.ItemsSource = UnitOfWork.BusRepository.GetAll().OrderBy(b => b.BusId).ToList();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            BusStationContext.Dispose();
        }
    }
}
