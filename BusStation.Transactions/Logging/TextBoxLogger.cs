﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusStation.Transactions.Logging
{
    public class TextBoxLogger : ILogger
    {
        private readonly string _categoryName;
        private readonly TextBoxWriter _textBoxWriter;

        public TextBoxLogger(string categoryName, TextBoxWriter textBoxWriter)
        {
            _categoryName = categoryName;
            _textBoxWriter = textBoxWriter;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            // You can implement scope if needed
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            // Adjust log level filtering as needed
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            // Format the log message and write to TextBox
            var logMessage = formatter(state, exception);
            _textBoxWriter.WriteLine($"{DateTime.Now} [{logLevel}] ({_categoryName}) {logMessage}");
        }
    }
}
