﻿using Microsoft.Extensions.Logging;

namespace BusStation.Transactions.Logging
{
    public class TextBoxLoggerProvider : ILoggerProvider
    {
        private readonly TextBoxWriter _textBoxWriter;

        public TextBoxLoggerProvider(TextBoxWriter textBoxWriter)
        {
            _textBoxWriter = textBoxWriter;
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new TextBoxLogger(categoryName, _textBoxWriter);
        }

        public void Dispose()
        {
            // Clean up resources if needed
        }
    }
}
