﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BusStation.Transactions.Logging
{
    public class TextBoxWriter : TextWriter
    {
        private readonly TextBox _textBox;

        public TextBoxWriter(TextBox textBox)
        {
            _textBox = textBox;
        }

        public override void Write(char value)
        {
            _textBox.Dispatcher.Invoke(() => _textBox.AppendText(value.ToString()));
        }

        public override void Write(string value)
        {
            _textBox.Dispatcher.Invoke(() => _textBox.AppendText(value));
        }

        public override Encoding Encoding => Encoding.UTF8;
    }
}
